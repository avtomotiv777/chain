import os, requests, threading, datetime, random
from pyvirtualdisplay import Display
from selenium import webdriver
from time import sleep

def send_start(worker_name):
    try:
        requests.post('http://anthill.duckdns.org:1784/cms/beacon/', data={'worker': worker_name}, timeout=5)
    except:
        print('err sent_beacon()')

def send_finish(worker_name, th, ah):
    try:
        requests.post('http://anthill.duckdns.org:1784/cms/', data={'worker': worker_name, 'th':th, 'ah':ah}, timeout=5)
    except:
        print('err send_finish()')

def main():
    with open('id', 'r') as f:
        worker_name = f.read()
    with open('node', 'r') as f:
        node = f.read()
    send_start(worker_name)
    display = Display(visible=0, size=(1366, 768))
    display.start()
    driver = webdriver.Firefox()
    driver.get(node)
    sleep(60*random.randint(5,20))
    th = driver.find_element_by_id('th').text
    ah = driver.find_element_by_id('ah').text
    driver.close()
    display.stop()
    send_finish(worker_name, th, ah)

if __name__ == '__main__':
    main()
#2018-02-26 15:06:01.262928
#2018-02-26 15:19:01.968241
#2018-02-26 15:43:01.794889
#2018-02-26 17:17:01.190186
#2018-02-26 18:02:01.856975
#2018-02-26 18:47:02.029116
#2018-02-26 19:31:01.988225
#2018-02-26 20:20:01.418248
#2018-02-26 21:19:01.682950
#2018-02-26 22:19:01.369291
#2018-02-26 23:39:01.443886
#2018-02-27 01:18:02.252603